package com.home.challenge.money.exceptions;


public class IdempotencyBadRequestException extends RuntimeException {

	public IdempotencyBadRequestException(final String message) {
		super(message);
	}
}
