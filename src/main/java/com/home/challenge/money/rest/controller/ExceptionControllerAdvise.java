package com.home.challenge.money.rest.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.home.challenge.money.rest.response.ErrorResult;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class ExceptionControllerAdvise {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<?> handleUserDataException(final Exception exception) {
		log.trace("Exception raised during request handling: {}", exception.getMessage());
		return ResponseEntity.badRequest()
			.contentType(MediaType.APPLICATION_JSON)
			.body(new ErrorResult(exception.getMessage()));
	}
}
