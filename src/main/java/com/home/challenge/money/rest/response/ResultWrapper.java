package com.home.challenge.money.rest.response;



public record ResultWrapper<T>(T result) {

}
