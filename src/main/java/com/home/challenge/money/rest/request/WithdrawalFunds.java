package com.home.challenge.money.rest.request;


import java.util.UUID;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;

import lombok.Data;

@Data
public class WithdrawalFunds {

	private final UUID id;
	private final Account source;
	private final String address;
	private final Money money;
}
