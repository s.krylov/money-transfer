package com.home.challenge.money.rest.controller;

import java.util.UUID;
import java.util.function.Function;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.rest.Headers;
import com.home.challenge.money.rest.request.InternalFundsMovement;
import com.home.challenge.money.rest.request.OperationId;
import com.home.challenge.money.rest.request.TopUpBalance;
import com.home.challenge.money.rest.request.WithdrawalFunds;
import com.home.challenge.money.rest.response.ResultWrapper;
import com.home.challenge.money.service.MoneyTransferFacade;

import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping("/money")
@RequiredArgsConstructor
public class TransferRestController {

	private final MoneyTransferFacade moneyTransferFacade;

	@RequestMapping(path = "/transfer", method = RequestMethod.POST)
	public Object transfer(@RequestBody InternalFundsMovement internalFundsMovement) {
		return prepareHandlerWithIdempotencyHeader(moneyTransferFacade::transfer)
			.apply(internalFundsMovement);
	}

	@RequestMapping(path = "/topup", method = RequestMethod.POST)
	public Object topUp(@RequestBody TopUpBalance topUpBalance) {
		return prepareHandlerWithIdempotencyHeader(moneyTransferFacade::topUp)
			.apply(topUpBalance);
	}

	@RequestMapping(path = "/withdraw", method = RequestMethod.POST)
	public Object withdraw(@RequestBody WithdrawalFunds withdrawalFunds) {
		return prepareHandlerWithIdempotencyHeader(moneyTransferFacade::withdraw)
			.apply(withdrawalFunds);
	}

	@RequestMapping(path = "/operation/{id}", method = RequestMethod.GET)
	public Object findOperation(@PathVariable UUID id) {
		return moneyTransferFacade.findOperation(new OperationId(id))
			.map(ResultWrapper::new)
			.map(ResponseEntity::ok)
			.orElseGet(() -> ResponseEntity.notFound().build());
	}

	@RequestMapping(path = "/account/{number}/operations", method = RequestMethod.GET)
	public Object getFundsMovement(@PathVariable String number) {
		return new ResultWrapper<>(moneyTransferFacade.getFundsMovement(new Account(number)));
	}

	@RequestMapping(path = "/account/{number}/balance", method = RequestMethod.GET)
	public Object calculateBalance(@PathVariable String number) {
		return new ResultWrapper<>(moneyTransferFacade.calculateBalance(new Account(number)));
	}

	private static <T, R extends Operation> Function<T, Object> prepareHandlerWithIdempotencyHeader(final Function<T, R> handler) {
		return handler.andThen(result -> ResponseEntity.ok()
				.header(Headers.IDEMPOTENCY, result.getId().toString())
				.body(new ResultWrapper<>(result)));
	}
}
