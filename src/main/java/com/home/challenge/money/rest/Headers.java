package com.home.challenge.money.rest;


import lombok.experimental.UtilityClass;

@UtilityClass
public class Headers {

	public static final String IDEMPOTENCY = "Idempotency-Key";
}
