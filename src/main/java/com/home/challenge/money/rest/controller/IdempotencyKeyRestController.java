package com.home.challenge.money.rest.controller;

import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.home.challenge.money.rest.Headers;
import com.home.challenge.money.rest.response.ResultWrapper;

import lombok.val;


@RestController
@RequestMapping("idempotency-key")
public class IdempotencyKeyRestController {

	@GetMapping
	public Object getIdempotencyKey() {
		val uuid = UUID.randomUUID();
		return ResponseEntity.ok()
			.header(Headers.IDEMPOTENCY, uuid.toString())
			.body(new ResultWrapper<>(uuid));
	}
}
