package com.home.challenge.money.rest.response;


public record ErrorResult(String errorMessage) {
}
