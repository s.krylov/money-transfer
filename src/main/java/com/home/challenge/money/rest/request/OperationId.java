package com.home.challenge.money.rest.request;


import java.util.UUID;

import lombok.Data;

@Data
public class OperationId {
	private final UUID id;
}
