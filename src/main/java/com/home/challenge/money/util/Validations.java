package com.home.challenge.money.util;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Validations {

	/**
	 * Require amount to be present and having positive value. Otherwise, it throws IllegalArgumentException
	 * @param money money
	 * @param errorMessage error message in case check fails
	 * @return original money-object if check passed
	 */
	public static Money requirePositiveAmount(final Money money, final String errorMessage) {
		Optional.ofNullable(money)
			.map(Money::amount)
			.filter(amount -> amount.compareTo(BigDecimal.ZERO) > 0)
			.orElseThrow(() -> new IllegalArgumentException(errorMessage));
		return money;
	}

	/**
	 * Require account to be present and having not-blank number. Otherwise, it throws IllegalArgumentException
	 * @param account account
	 * @param errorMessage error message in case check fails
	 * @return original money-object if check passed
	 */
	public static Account requireAccountNumberNotBlank(final Account account, final String errorMessage) {
		Optional.ofNullable(account)
			.map(Account::number)
			.filter(Predicate.not(String::isBlank))
			.orElseThrow(() -> new IllegalArgumentException(errorMessage));
		return account;
	}

	/**
	 * Require source string to be not-blank. Otherwise, it throws IllegalArgumentException
	 * @param source source-string
	 * @param errorMessage error message in case check fails
	 * @return original source-string if check passed
	 */
	public static String requireNotBlank(final String source, final String errorMessage) {
		Optional.ofNullable(source)
			.filter(Predicate.not(String::isBlank))
			.orElseThrow(() -> new IllegalArgumentException(errorMessage));
		return source;
	}

	/**
	 * Require source account not be equals to destination account. Otherwise, it throws IllegalArgumentException
	 * @param source source account
	 * @param destination destination account
	 * @param errorMessage error message in case check fails
	 */
	public static void requireNotTheSameAccounts(final Account source, final Account destination, final String errorMessage) {
		if (Objects.equals(source, destination)) {
			throw new IllegalArgumentException(errorMessage);
		}
	}
}
