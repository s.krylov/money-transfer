/*
 * Copyright (c) 2024 Payoneer Germany GmbH. All rights reserved.
 */
package com.home.challenge.money.util;


import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.home.challenge.money.pojo.BalanceChange;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.OperationResult;

import lombok.experimental.UtilityClass;

@UtilityClass
public class MoneyUtils {

	private static final Money NO_MONEY = new Money(BigDecimal.ZERO);

	/**
	 * Calculate effective balance based on passed operation.
	 * @param operations operations list
	 * @return money balance
	 */
	public static Money calculateEffectiveBalance(final List<BalanceChange> operations) {
		return operations == null ? NO_MONEY : operations.stream()
			.filter(balanceChange -> balanceChange.getResult() != OperationResult.FAILED)
			.map(BalanceChange::getAmount)
			.map(Money::amount)
			.collect(Collectors.collectingAndThen(Collectors.reducing(BigDecimal.ZERO, BigDecimal::add), Money::new));
	}
}
