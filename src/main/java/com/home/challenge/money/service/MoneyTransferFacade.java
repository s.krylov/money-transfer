package com.home.challenge.money.service;


import java.util.List;
import java.util.Optional;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;
import com.home.challenge.money.rest.request.OperationId;
import com.home.challenge.money.rest.request.InternalFundsMovement;
import com.home.challenge.money.rest.request.TopUpBalance;
import com.home.challenge.money.rest.request.WithdrawalFunds;

/**
 * Money transfer service
 */
public interface MoneyTransferFacade {

	/**
	 * transfer money between source and destination account
	 *
	 * @param internalFundsMovement internal fund movement data
	 * @return transfer operation result
	 */
	TransferOperation transfer(InternalFundsMovement internalFundsMovement);

	/**
	 * Top up balance
	 *
	 * @param topUpBalance top up balance data
	 * @return operation result
	 */
	TopUpOperation topUp(TopUpBalance topUpBalance);

	/**
	 * Withdraw money from account balance
	 *
	 * @param withdrawalFunds withdrawal funds data id
	 * @return operation result
	 */
	WithdrawalOperation withdraw(WithdrawalFunds withdrawalFunds);

	/**
	 * Find operation by id
	 * @param id operation id
	 * @return optional of operation
	 */
	Optional<Operation> findOperation(OperationId id);

	/**
	 * Get funds movement list
	 * @param account account
	 * @return list of funds movement
	 */
	List<Operation> getFundsMovement(Account account);

	/**
	 * Calculate account balance
	 * @param account account
	 * @return balance
	 */
	Money calculateBalance(Account account);
}
