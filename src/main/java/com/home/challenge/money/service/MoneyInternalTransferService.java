package com.home.challenge.money.service;


import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;

/**
 * Money transfer service
 */
public interface MoneyInternalTransferService {

	/**
	 * transfer money between source and destination account
	 *
	 * @param id operation id
	 * @param money money
	 * @param source source account
	 * @param destination destination account
	 * @return transfer operation
	 */
	TransferOperation transfer(UUID id, Money money, Account source, Account destination);

	/**
	 * Top up balance
	 *
	 * @param id operation id
	 * @param money money
	 * @param destination destination account
	 * @return Top up operation
	 */
	TopUpOperation topUp(UUID id, Money money, Account destination);

	/**
	 * Withdraw money from account balance
	 *
	 * @param id operation id
	 * @param money money
	 * @param source source account
	 * @return Withdrawal operation
	 */
	WithdrawalOperation withdraw(UUID id, Money money, Account source);

	/**
	 * Find operation by id
	 * @param id operation id
	 * @return optional of operation
	 */
	Optional<Operation> findOperation(UUID id);

	/**
	 * Get funds movement list
	 * @param account account
	 * @return list of funds movement
	 */
	List<Operation> getFundsMovement(Account account);

	/**
	 * Calculate account balance
	 * @param account account
	 * @return balance
	 */
	Money calculateBalance(Account account);
}
