package com.home.challenge.money.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.OperationResult;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;
import com.home.challenge.money.rest.request.InternalFundsMovement;
import com.home.challenge.money.rest.request.OperationId;
import com.home.challenge.money.rest.request.TopUpBalance;
import com.home.challenge.money.rest.request.WithdrawalFunds;
import com.home.challenge.money.service.MoneyInternalTransferService;
import com.home.challenge.money.service.MoneyTransferFacade;
import com.home.challenge.money.service.WithdrawalService;
import com.home.challenge.money.service.WithdrawalService.WithdrawalId;
import com.home.challenge.money.util.Validations;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;


@Service
@RequiredArgsConstructor
@Slf4j
public class MoneyTransferFacadeImpl implements MoneyTransferFacade {

	private final MoneyInternalTransferService moneyInternalTransferService;
	private final WithdrawalService withdrawalService;

	// todo extra response mapping can be added
	@Override
	public TransferOperation transfer(final InternalFundsMovement internalFundsMovement) {
		log.trace("Money transfer started for internalFundsMovement: {}", internalFundsMovement);
		val id = internalFundsMovement.getId() == null ? UUID.randomUUID() : internalFundsMovement.getId();
		val source = Validations.requireAccountNumberNotBlank(internalFundsMovement.getSource(), "Source account is required");
		val destination = Validations.requireAccountNumberNotBlank(internalFundsMovement.getDestination(), "Destination account is required");
		Validations.requireNotTheSameAccounts(source, destination, "Source and destination are the same");
		val money = Validations.requirePositiveAmount(internalFundsMovement.getMoney(), "Money amount should be positive");
		val result = moneyInternalTransferService.transfer(id, money, source, destination);
		log.trace("Money transfer finished successfully for internalFundsMovement: {}, id: {}", internalFundsMovement, id);
		return result;
	}

	@Override
	public TopUpOperation topUp(final TopUpBalance topUpBalance) {
		log.trace("Top up balance started for topUpBalance: {}", topUpBalance);
		val id = topUpBalance.getId() == null ? UUID.randomUUID() : topUpBalance.getId();
		val destination = Validations.requireAccountNumberNotBlank(topUpBalance.getDestination(), "Destination account is required");
		val money = Validations.requirePositiveAmount(topUpBalance.getMoney(), "Money amount should be positive");
		val result = moneyInternalTransferService.topUp(id, money, destination);
		log.trace("Top up balance finished successfully for topUpBalance: {}, id: {}", topUpBalance, id);
		return result;
	}

	@Override
	public WithdrawalOperation withdraw(final WithdrawalFunds withdrawalFunds) {
		log.trace("Funds withdrawal started for withdrawalFunds: {}", withdrawalFunds);
		val id = withdrawalFunds.getId() == null ? UUID.randomUUID() : withdrawalFunds.getId();
		val source = Validations.requireAccountNumberNotBlank(withdrawalFunds.getSource(), "Source account is required");
		val address = Validations.requireNotBlank(withdrawalFunds.getAddress(), "Withdrawal address is required");
		val money = Validations.requirePositiveAmount(withdrawalFunds.getMoney(), "Money amount should be positive");
		val result = moneyInternalTransferService.withdraw(id, money, source);
		if (result.isPartiallyFailed()) {
			result.addResult(OperationResult.FAILED);
		} else {
			withdrawalService.requestWithdrawal(new WithdrawalId(id), new WithdrawalService.Address(address), money);
			result.addResult(() -> OperationResult.valueOf(withdrawalService.getRequestState(new WithdrawalId(id)).name()));
		}
		log.trace("Funds withdrawal finished successfully for withdrawalFunds: {}, id: {}", withdrawalFunds, id);
		return result;
	}

	@Override
	public Optional<Operation> findOperation(final OperationId id) {
		log.trace("Operation search started for id: {}", id);
		val uuid = Objects.requireNonNull(id.getId(), "id is required");
		val result = moneyInternalTransferService.findOperation(uuid);
		log.trace("Operation search successfully finished for id: {}", id);
		return result;
	}

	@Override
	public List<Operation> getFundsMovement(final Account account) {
		log.trace("Funds movement query started for account: {}", account);
		Validations.requireAccountNumberNotBlank(account, "Account is required");
		val result = moneyInternalTransferService.getFundsMovement(account);

		log.trace("Funds movement query successfully finished for account: {}", account);
		return result;
	}

	@Override
	public Money calculateBalance(final Account account) {
		log.trace("Balance calculation started for account: {}", account);
		Validations.requireAccountNumberNotBlank(account, "Account is required");
		val result = moneyInternalTransferService.calculateBalance(account);

		log.trace("Balance calculation successfully finished for account: {}", account);
		return result;
	}
}
