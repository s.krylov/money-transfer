package com.home.challenge.money.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.exceptions.IdempotencyBadRequestException;
import com.home.challenge.money.pojo.BalanceChange;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.OperationResult;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;
import com.home.challenge.money.service.MoneyInternalTransferService;
import com.home.challenge.money.util.MoneyUtils;

import lombok.val;


@Service
public class MoneyInternalTransferServiceImpl implements MoneyInternalTransferService {

	private final Map<UUID, Operation> uuidToOperationMap;
	private final Map<Account, List<BalanceChange>> accountToBalanceChangeMap;

	public MoneyInternalTransferServiceImpl() {
		this(new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
	}

	MoneyInternalTransferServiceImpl(final Map<UUID, Operation> uuidToOperationMap,
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap) {
		this.uuidToOperationMap = uuidToOperationMap;
		this.accountToBalanceChangeMap = accountToBalanceChangeMap;
	}

	@Override
	public TransferOperation transfer(final UUID id, final Money money, final Account source, final Account destination) {
		val operation = new TransferOperation(id, money, source, destination);
		val current = uuidToOperationMap.putIfAbsent(id, operation);
		if (current != null) {
			if (current.equals(operation)) {
				return (TransferOperation) current;
			}
			throw new IdempotencyBadRequestException("Details of current operation are differs with original one");
		}

		accountToBalanceChangeMap.compute(operation.getSource(), (account, operations) -> {
			val newOperations = operations == null ? new CopyOnWriteArrayList<BalanceChange>() : operations;
			val balanceChange = operation.createCreditOperation();
			val balance = MoneyUtils.calculateEffectiveBalance(operations);
			newOperations.add(balanceChange);
			balanceChange.setResult(
				balance.compareTo(balanceChange.getAmount().negate()) > 0 ? OperationResult.COMPLETED : OperationResult.FAILED);

			return newOperations;
		});

		accountToBalanceChangeMap.compute(operation.getDestination(), (account, operations) -> {
			val newOperations = operations == null ? new CopyOnWriteArrayList<BalanceChange>() : operations;
			val balanceChange = operation.createDebitOperation();
			newOperations.add(balanceChange);
			balanceChange.setResult(OperationResult.COMPLETED);

			return newOperations;
		});

		return operation;
	}

	@Override
	public TopUpOperation topUp(final UUID id, final Money money, final Account destination) {
		val operation = new TopUpOperation(id, money, destination);
		val current = uuidToOperationMap.putIfAbsent(id, operation);
		if (current != null) {
			if (current.equals(operation)) {
				return (TopUpOperation) current;
			}
			throw new IdempotencyBadRequestException("Details of current operation are differs with original one");
		}

		accountToBalanceChangeMap.compute(operation.getDestination(), (account, operations) -> {
			val newOperations = operations == null ? new CopyOnWriteArrayList<BalanceChange>() : operations;
			val balanceChange = operation.createDebitOperation();
			newOperations.add(balanceChange);
			balanceChange.setResult(OperationResult.COMPLETED);

			return newOperations;
		});

		return operation;
	}

	@Override
	public WithdrawalOperation withdraw(final UUID id, final Money money, final Account source) {
		val operation = new WithdrawalOperation(id, money, source);
		val current = uuidToOperationMap.putIfAbsent(id, operation);
		if (current != null) {
			if (current.equals(operation)) {
				return (WithdrawalOperation) current;
			}
			throw new IdempotencyBadRequestException("Details of current operation are differs with original one");
		}

		accountToBalanceChangeMap.compute(operation.getSource(), (account, operations) -> {
			val newOperations = operations == null ? new CopyOnWriteArrayList<BalanceChange>() : operations;
			val balanceChange = operation.createCreditOperation();
			val balance = MoneyUtils.calculateEffectiveBalance(operations);
			newOperations.add(balanceChange);
			balanceChange.setResult(
				balance.compareTo(balanceChange.getAmount().negate()) > 0 ? OperationResult.COMPLETED : OperationResult.FAILED);

			return newOperations;
		});

		return operation;
	}

	@Override
	public Optional<Operation> findOperation(final UUID id) {
		return Optional.ofNullable(uuidToOperationMap.get(id));
	}

	@Override
	public List<Operation> getFundsMovement(final Account account) {
		return Optional.ofNullable(accountToBalanceChangeMap.get(account))
			.stream()
			.flatMap(List::stream)
			.map(BalanceChange::getParent)
			.collect(Collectors.toList());
	}

	@Override
	public Money calculateBalance(final Account account) {
		return Optional.ofNullable(accountToBalanceChangeMap.get(account))
			.map(MoneyUtils::calculateEffectiveBalance)
			.orElseGet(() -> new Money(BigDecimal.ZERO));
	}
}
