package com.home.challenge.money.pojo;

import java.util.UUID;
import java.util.function.Supplier;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true, of = {"source"})
public class WithdrawalOperation extends Operation {

	private final Account source;

	private final LazyCompoundOperationResult lazyCompoundOperationResult;

	public WithdrawalOperation(final UUID id, final Money amount, final Account source) {
		super(id, amount, OperationType.WITHDRAWAL, null);
		this.source = source;
		this.lazyCompoundOperationResult = LazyCompoundOperationResult.createProcessing(2);
	}

	public BalanceChange createCreditOperation() {
		return new BalanceChange(this, source, getAmount().negate());
	}

	public boolean isPartiallyFailed() {
		return lazyCompoundOperationResult.hasValue(OperationResult.FAILED);
	}

	@Override
	public void addResult(final OperationResult result) {
		lazyCompoundOperationResult.addResult(result);
	}

	public void addResult(final Supplier<OperationResult> result) {
		lazyCompoundOperationResult.addResult(result);
	}

	@Override
	public OperationResult getResult() {
		return lazyCompoundOperationResult.getResult();
	}
}
