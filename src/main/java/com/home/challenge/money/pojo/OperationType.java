package com.home.challenge.money.pojo;


public enum OperationType {
	TRANSFER,
	WITHDRAWAL,
	TOPUP
}
