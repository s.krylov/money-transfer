package com.home.challenge.money.pojo;


import java.util.function.Supplier;
import java.util.stream.IntStream;

import lombok.val;

public class LazyCompoundOperationResult {

	private final Supplier[] results;
	private int pos;
	private LazyCompoundOperationResult(final int size, final OperationResult initial) {
		this.results = IntStream.range(0, size)
				.mapToObj(ignore -> ((Supplier<OperationResult>) () -> initial))
				.toArray(Supplier[]::new);
		this.pos = 0;
	}

	public static LazyCompoundOperationResult createProcessing(final int size) {
		return new LazyCompoundOperationResult(size, OperationResult.PROCESSING);
	}

	public void addResult(final OperationResult result) {
		addResult(() -> result);
	}

	public void addResult(final Supplier<OperationResult> result) {
		if (pos >= results.length) {
			return;
		}
		results[pos++] = result;
	}


	public OperationResult getResult() {
		return hasValue(OperationResult.PROCESSING) ? OperationResult.PROCESSING :
			hasValue(OperationResult.FAILED) ? OperationResult.FAILED : OperationResult.COMPLETED;
	}

	boolean hasValue(final OperationResult expected) {
		for (val operationResult : results) {
			if (operationResult.get() == expected) {
				return true;
			}
		}
		return false;
	}
}
