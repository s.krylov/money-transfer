package com.home.challenge.money.pojo;


import java.math.BigDecimal;
import java.util.Comparator;


public record Money(BigDecimal amount) implements Comparable<Money> {

	private static final Comparator<Money> comparator = Comparator.comparing(Money::amount);

	public Money negate() {
		return new Money(this.amount.negate());
	}

	@Override
	public int compareTo(final Money o) {
		return comparator.compare(this, o);
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Money money = (Money) o;

		return amount.compareTo(money.amount) == 0;
	}

}
