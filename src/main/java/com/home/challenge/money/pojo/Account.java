package com.home.challenge.money.pojo;

public record Account(String number) {
	public Account {
		number = number == null ? null : number.trim();
	}
}
