package com.home.challenge.money.pojo;

import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.Getter;


@Getter
@EqualsAndHashCode(callSuper = true, of = {"source", "destination"})
public class TransferOperation extends Operation {

	private final Account source;
	private final Account destination;

	public TransferOperation(final UUID id, final Money amount, final Account source, final Account destination) {
		super(id, amount, OperationType.TRANSFER, CompoundOperationResult.createProcessing(2));
		this.source = source;
		this.destination = destination;
	}

	public BalanceChange createCreditOperation() {
		return new BalanceChange(this, source, getAmount().negate());
	}

	public BalanceChange createDebitOperation() {
		return new BalanceChange(this, destination, getAmount());
	}
}
