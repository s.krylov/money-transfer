package com.home.challenge.money.pojo;

import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode(callSuper = true, of = {"destination"})
public class TopUpOperation extends Operation {

	private final Account destination;

	public TopUpOperation(final UUID id, final Money amount, final Account destination) {
		super(id, amount, OperationType.TOPUP, CompoundOperationResult.createProcessing(1));
		this.destination = destination;
	}

	public BalanceChange createDebitOperation() {
		return new BalanceChange(this, destination, getAmount());
	}
}
