package com.home.challenge.money.pojo;


import java.util.UUID;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
@EqualsAndHashCode(of = {"id", "amount", "type"})
public abstract class Operation {

	@Getter
	private final UUID id;
	@Getter
	private final Money amount;
	@Getter
	private final OperationType type;

	private final CompoundOperationResult compoundOperationResult;

	public void addResult(final OperationResult result) {
		compoundOperationResult.addResult(result);
	}
	public OperationResult getResult() {
		return compoundOperationResult.getResult();
	}
}
