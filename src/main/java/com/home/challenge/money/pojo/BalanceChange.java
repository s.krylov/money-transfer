package com.home.challenge.money.pojo;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class BalanceChange {

	private final Operation parent;
	private final Account account;
	private final Money amount;

	public OperationResult getResult() {
		return parent.getResult();
	}

	public void setResult(final OperationResult result) {
		parent.addResult(result);
	}
}
