package com.home.challenge.money.pojo;

import java.util.Arrays;

import lombok.val;

public class CompoundOperationResult {
	private final OperationResult[] results;
	private int pos;
	private CompoundOperationResult(final int size, final OperationResult initial) {
		this.results = new OperationResult[size];
		this.pos = 0;
		Arrays.fill(results, initial);
	}

	public static CompoundOperationResult createProcessing(final int size) {
		return new CompoundOperationResult(size, OperationResult.PROCESSING);
	}

	public void addResult(final OperationResult result) {
		if (pos >= results.length) {
			return;
		}
		results[pos++] = result;
	}

	public OperationResult getResult() {
		return hasValue(OperationResult.PROCESSING) ? OperationResult.PROCESSING :
			hasValue(OperationResult.FAILED) ? OperationResult.FAILED : OperationResult.COMPLETED;
	}

	private boolean hasValue(final OperationResult expected) {
		for (val operationResult : results) {
			if (operationResult == expected) {
				return true;
			}
		}
		return false;
	}
}
