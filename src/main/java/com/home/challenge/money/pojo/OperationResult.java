package com.home.challenge.money.pojo;


public enum OperationResult {
	PROCESSING,
	COMPLETED,
	FAILED
}
