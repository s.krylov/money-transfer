package com.home.challenge.money.pojo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.*;

import java.util.function.Supplier;
import java.util.stream.IntStream;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import lombok.val;

public class LazyCompoundOperationResultTest {

	@DataProvider
	public Object[][] getResultDataProvider() {
		return new Object[][] {
			{ 10, 3, 3, 4, OperationResult.PROCESSING },
			{ 10, 10, 0, 0, OperationResult.PROCESSING },
			{ 10, 5, 0, 5, OperationResult.PROCESSING },
			{ 10, 5, 5, 0, OperationResult.PROCESSING },
			{ 20, 0, 10, 10, OperationResult.FAILED },
			{ 20, 0, 0, 20, OperationResult.FAILED },
			{ 20, 0, 20, 0, OperationResult.COMPLETED },
		};
	}

	@Test(dataProvider = "getResultDataProvider")
	public void testGetResult(final int size, final int processing, final int completed, final int failed, final OperationResult expected) {
		val lazyCompoundOperationResult = LazyCompoundOperationResult.createProcessing(size);
		setOperationResult(lazyCompoundOperationResult, OperationResult.PROCESSING, processing);
		setOperationResult(lazyCompoundOperationResult, OperationResult.COMPLETED, completed);
		setOperationResult(lazyCompoundOperationResult, OperationResult.FAILED, failed);
		assertThat(lazyCompoundOperationResult.getResult()).isEqualTo(expected);
	}

	private static void setOperationResult(final LazyCompoundOperationResult lazyCompoundOperationResult, final OperationResult result,
		final int amount) {
		IntStream.range(0, amount).forEach(ignore -> {
			val mocked = (Supplier<OperationResult>) mock(Supplier.class);
			doReturn(result).when(mocked).get();
			lazyCompoundOperationResult.addResult(mocked);
		});
	}
}