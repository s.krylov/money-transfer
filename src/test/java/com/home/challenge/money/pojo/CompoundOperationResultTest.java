package com.home.challenge.money.pojo;

import static org.assertj.core.api.Assertions.assertThat;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import lombok.val;

public class CompoundOperationResultTest {

	@DataProvider
	public Object[][] getResultDataProvider() {
		return new Object[][] {
			{10, 3, 3, 4, OperationResult.PROCESSING},
			{10, 10, 0, 0, OperationResult.PROCESSING},
			{10, 5, 0, 5, OperationResult.PROCESSING},
			{10, 5, 5, 0, OperationResult.PROCESSING},
			{20, 0, 10, 10, OperationResult.FAILED},
			{20, 0, 0, 20, OperationResult.FAILED},
			{20, 0, 20, 0, OperationResult.COMPLETED},
		};
	}

	@Test(dataProvider = "getResultDataProvider")
	public void testGetResult(final int size, final int processing, final int completed, final int failed,
		final OperationResult expected) {
		val compoundOperationResult = CompoundOperationResult.createProcessing(size);
		setOperationResult(compoundOperationResult, OperationResult.PROCESSING, processing);
		setOperationResult(compoundOperationResult, OperationResult.COMPLETED, completed);
		setOperationResult(compoundOperationResult, OperationResult.FAILED, failed);
		assertThat(compoundOperationResult.getResult()).isEqualTo(expected);
	}


	private static void setOperationResult(final CompoundOperationResult compoundOperationResult, final OperationResult result,
		final int amount) {
		for (int i = 0; i < amount; i++) {
			compoundOperationResult.addResult(result);
		}
	}
}