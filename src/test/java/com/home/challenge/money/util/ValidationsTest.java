package com.home.challenge.money.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.math.BigDecimal;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;

import lombok.val;


public class ValidationsTest {

	@DataProvider
	public Object[][] requirePositiveAmountDataProvider() {
		return new Object[][] {
			{new Money(new BigDecimal("0.000001")), "will not thrown"},
			{new Money(new BigDecimal("20")), "test"},
			{new Money(BigDecimal.ONE), null},
			{new Money(BigDecimal.TWO), "null"},
			{new Money(BigDecimal.TEN), "ten"},
		};
	}

	@Test(dataProvider = "requirePositiveAmountDataProvider")
	public void testRequirePositiveAmount(final Money money, final String errorMessage) {
		val actual = Validations.requirePositiveAmount(money, errorMessage);
		assertThat(actual).isEqualTo(money);
	}

	@DataProvider
	public Object[][] requirePositiveAmountValidationFailedDataProvider() {
		return new Object[][] {
			{null, "no money"},
			{new Money(new BigDecimal("-0.000001")), "will not thrown"},
			{new Money(new BigDecimal("-20")), "test"},
			{new Money(BigDecimal.ONE.negate()), null},
			{new Money(BigDecimal.TWO.negate()), "null"},
			{new Money(BigDecimal.TEN.negate()), "ten"},
		};
	}

	@Test(dataProvider = "requirePositiveAmountValidationFailedDataProvider")
	public void testRequirePositiveAmountValidationFailed(final Money money, final String errorMessage) {
		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> Validations.requirePositiveAmount(money, errorMessage))
			.withMessage(errorMessage);
	}

	@DataProvider
	public Object[][] requireAccountNumberNotBlankDataProvider() {
		return new Object[][] {
			{new Account("abc"), "no error thrown"},
			{new Account("  abc  "), "  no error thrown  "},
			{new Account("null"), "null"},
		};
	}

	@Test(dataProvider = "requireAccountNumberNotBlankDataProvider")
	public void testRequireAccountNumberNotBlank(final Account account, final String errorMessage) {
		val actual = Validations.requireAccountNumberNotBlank(account, errorMessage);
		assertThat(actual).isEqualTo(account);
	}

	@DataProvider
	public Object[][] requireAccountNumberNotBlankValidationFailedDataProvider() {
		return new Object[][] {
			{null, "null object"},
			{new Account(null), "null account number"},
			{new Account(""), "empty account number"},
			{new Account("   "), "blank account number"},
		};
	}

	@Test(dataProvider = "requireAccountNumberNotBlankValidationFailedDataProvider")
	public void testRequireAccountNumberNotBlankValidationFailed(final Account account, final String errorMessage) {
		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> Validations.requireAccountNumberNotBlank(account, errorMessage))
			.withMessage(errorMessage);
	}

	@DataProvider
	public Object[][] requireNotBlankDataProvider() {
		return new Object[][] {
			{"definitely not blank string", "no error thrown"},
			{"   definitely not blank string   ", "  no error thrown   "},
			{"null", "success"},
		};
	}

	@Test(dataProvider = "requireNotBlankDataProvider")
	public void testRequireNotBlank(final String source, final String errorMessage) {
		val actual = Validations.requireNotBlank(source, errorMessage);
		assertThat(actual).isEqualTo(source);
	}

	@DataProvider
	public Object[][] requireNotBlankValidationFailedDataProvider() {
		return new Object[][] {
			{null, "null value"},
			{"", "empty value"},
			{"  ", "blank value"},
		};
	}

	@Test(dataProvider = "requireNotBlankValidationFailedDataProvider")
	public void testRequireNotBlankValidationFailed(final String source, final String errorMessage) {
		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> Validations.requireNotBlank(source, errorMessage))
			.withMessage(errorMessage);
	}


	///////////////

	@DataProvider
	public Object[][] requireNotTheSameAccountsDataProvider() {
		return new Object[][] {
			{new Account("abc"), new Account("def"), "no error thrown"},
			{new Account(null), new Account("qqq"), "null account number, no error thrown"},
			{null, new Account("www"), "null account, no error thrown"},
			{new Account("qqq"), new Account(null), "null account number, no error thrown"},
			{new Account("www"), null, "null account, no error thrown"},
			{null, new Account(null), "null account and null account number, no error thrown"},
			{new Account(null), null, "null account and null account number, no error thrown"},
		};
	}

	@Test(dataProvider = "requireNotTheSameAccountsDataProvider")
	public void testRequireNotTheSameAccounts(final Account source, final Account destination, final String errorMessage) {
		Validations.requireNotTheSameAccounts(source, destination, errorMessage);
	}

	@DataProvider
	public Object[][] requireNotTheSameAccountsValidationFailedDataProvider() {
		return new Object[][] {
			{new Account("abc"), new Account("abc"), "same values"},
			{new Account("abc"), new Account("   abc "), "same trimmed values"},
			{new Account(null), new Account(null), "same null account number values"},
			{null, null, "same null account number values"},
		};
	}

	@Test(dataProvider = "requireNotTheSameAccountsValidationFailedDataProvider")
	public void testRequireNotTheSameAccountsValidationFailed(final Account source, final Account destination, final String errorMessage) {
		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> Validations.requireNotTheSameAccounts(source, destination, errorMessage))
			.withMessage(errorMessage);
	}
}