package com.home.challenge.money.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.home.challenge.money.TestObjectsFactory;
import com.home.challenge.money.pojo.BalanceChange;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.OperationResult;
import com.home.challenge.money.pojo.OperationType;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;

import lombok.val;

public class MoneyUtilsTest {

	@DataProvider
	public Object[][] calculateEffectiveBalanceDataProvider() {
		val topUpProcessingOperation3203 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "123", new BigDecimal("32.03"));
		val topUpCompletedOperation0105 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "321", new BigDecimal("1.05"));
		topUpCompletedOperation0105.addResult(OperationResult.COMPLETED);
		val topUpFailedOperation2265 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "222", new BigDecimal("22.65"));
		topUpFailedOperation2265.addResult(OperationResult.FAILED);

		val withdrawProcessing0205 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "qqq", new BigDecimal("2.05"));
		val withdrawCompleted2000 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "www", new BigDecimal("20.00"));
		withdrawCompleted2000.addResult(OperationResult.COMPLETED);
		withdrawCompleted2000.addResult(OperationResult.COMPLETED);
		val withdrawFailed5021 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "eee", new BigDecimal("50.21"));
		withdrawFailed5021.addResult(OperationResult.FAILED);
		withdrawFailed5021.addResult(OperationResult.FAILED);

		val transferProcessing1203 = (TransferOperation) TestObjectsFactory.createOperation(OperationType.TRANSFER, "aaa", new BigDecimal("12.03"));
		val transferCompleted0528 = (TransferOperation) TestObjectsFactory.createOperation(OperationType.TRANSFER, "www", new BigDecimal("05.28"));
		transferCompleted0528.addResult(OperationResult.COMPLETED);
		transferCompleted0528.addResult(OperationResult.COMPLETED);
		val transferFailed1099 = (TransferOperation) TestObjectsFactory.createOperation(OperationType.TRANSFER, "eee", new BigDecimal("10.99"));
		transferFailed1099.addResult(OperationResult.FAILED);
		transferFailed1099.addResult(OperationResult.FAILED);


		return new Object[][] {
			{ null, new Money(BigDecimal.ZERO) },
			{ Collections.emptyList(), new Money(BigDecimal.ZERO)},
			{ Arrays.asList(
				topUpFailedOperation2265.createDebitOperation(),
				withdrawFailed5021.createCreditOperation(),
				transferFailed1099.createCreditOperation()
			), new Money(BigDecimal.ZERO)},
			{ Arrays.asList(
				topUpProcessingOperation3203.createDebitOperation(),
				withdrawProcessing0205.createCreditOperation(),
				transferProcessing1203.createCreditOperation()
			), new Money(new BigDecimal("17.95"))},
			{ Arrays.asList(
				topUpCompletedOperation0105.createDebitOperation(),
				withdrawCompleted2000.createCreditOperation(),
				transferCompleted0528.createDebitOperation()
			), new Money(new BigDecimal("-13.67"))},
			{ Arrays.asList(
				topUpCompletedOperation0105.createDebitOperation(),
				topUpProcessingOperation3203.createDebitOperation(),
				topUpFailedOperation2265.createDebitOperation(),
				withdrawProcessing0205.createCreditOperation(),
				withdrawCompleted2000.createCreditOperation(),
				withdrawFailed5021.createCreditOperation(),
				transferProcessing1203.createCreditOperation(),
				transferCompleted0528.createDebitOperation(),
				transferFailed1099.createCreditOperation()
				), new Money(new BigDecimal("4.28"))},

			{ Arrays.asList(
				topUpCompletedOperation0105.createDebitOperation(),
				topUpProcessingOperation3203.createDebitOperation(),
				withdrawProcessing0205.createCreditOperation(),
				withdrawProcessing0205.createCreditOperation(),
				withdrawProcessing0205.createCreditOperation(),
				transferFailed1099.createDebitOperation(),
				transferFailed1099.createDebitOperation(),
				transferFailed1099.createDebitOperation(),
				transferCompleted0528.createCreditOperation()
			), new Money(new BigDecimal("21.65"))},
		};
	}

	@Test(dataProvider = "calculateEffectiveBalanceDataProvider")
	public void testCalculateEffectiveBalance(final List<BalanceChange> operations, final Money expected) {
		final Money result = MoneyUtils.calculateEffectiveBalance(operations);
		assertThat(result)
			.isEqualTo(expected);
	}
}