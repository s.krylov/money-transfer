package com.home.challenge.money;

import java.math.BigDecimal;
import java.util.UUID;

import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.OperationType;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;

import lombok.experimental.UtilityClass;
import lombok.val;

@UtilityClass
public class TestObjectsFactory {

	public static Operation createOperation(final OperationType type, final String number, final BigDecimal amount) {
		val uuid = UUID.randomUUID();
		val money = new Money(amount);
		return switch (type) {
			case TOPUP -> new TopUpOperation(uuid, money, new Account(number + "-test"));
			case WITHDRAWAL -> new WithdrawalOperation(uuid, money, new Account(number + "-test"));
			case TRANSFER -> new TransferOperation(uuid, money, new Account(number + "-source-test"), new Account(number + "-destination-test"));
		};
	}
}
