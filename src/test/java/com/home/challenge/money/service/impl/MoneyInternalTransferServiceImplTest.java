package com.home.challenge.money.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.testng.Assert.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.home.challenge.money.TestObjectsFactory;
import com.home.challenge.money.exceptions.IdempotencyBadRequestException;
import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.BalanceChange;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.OperationResult;
import com.home.challenge.money.pojo.OperationType;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;

import lombok.val;

public class MoneyInternalTransferServiceImplTest {

	private MoneyInternalTransferServiceImpl moneyInternalTransferService;

	@Test
	public void testTransfer() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "abc", new BigDecimal("12.21"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = UUID.randomUUID();
		val money = new Money(new BigDecimal("5.21"));
		val source = new Account("abc-test");
		val destination = new Account("cba-test");
		val expectedResult = new TransferOperation(id, money, source, destination);
		val creditOperation = expectedResult.createCreditOperation();
		creditOperation.setResult(OperationResult.COMPLETED);
		val debitOperation = expectedResult.createDebitOperation();
		debitOperation.setResult(OperationResult.COMPLETED);

		assertThat(moneyInternalTransferService.transfer(id, money, source, destination))
			.isEqualTo(expectedResult);
		assertThat(uuidToOperationMap)
			.hasSize(2)
			.extractingByKey(id)
			.isEqualTo(expectedResult);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(2);

		movementFundsAssertions
			.extractingByKey(source)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(topUpOperation.createDebitOperation(), creditOperation);

		movementFundsAssertions
			.extractingByKey(destination)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(debitOperation);
	}

	@Test
	public void testTransferNotEnoughBalance() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = UUID.randomUUID();
		val money = new Money(new BigDecimal("5.21"));
		val source = new Account("abc-test");
		val destination = new Account("cba-test");
		val expectedResult = new TransferOperation(id, money, source, destination);
		val creditOperation = expectedResult.createCreditOperation();
		creditOperation.setResult(OperationResult.FAILED);
		val debitOperation = expectedResult.createDebitOperation();
		debitOperation.setResult(OperationResult.COMPLETED);

		assertThat(moneyInternalTransferService.transfer(id, money, source, destination))
			.isEqualTo(expectedResult);
		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(id)
			.isEqualTo(expectedResult);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(2);

		movementFundsAssertions
			.extractingByKey(source)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(creditOperation);

		movementFundsAssertions
			.extractingByKey(destination)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(debitOperation);
	}

	@Test
	public void testTransferAlreadyCalculated() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val id = UUID.randomUUID();
		val money = new Money(new BigDecimal("5.21"));
		val source = new Account("abc-test");
		val destination = new Account("cba-test");

		val transferOperation = new TransferOperation(id, money, source, destination);
		val creditOperation = transferOperation.createCreditOperation();
		creditOperation.setResult(OperationResult.FAILED);
		val debitOperation = transferOperation.createDebitOperation();
		debitOperation.setResult(OperationResult.FAILED);

		uuidToOperationMap.put(transferOperation.getId(), transferOperation);
		accountToBalanceChangeMap.put(transferOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(debitOperation)));
		accountToBalanceChangeMap.put(transferOperation.getSource(), new CopyOnWriteArrayList<>(Collections.singletonList(creditOperation)));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		assertThat(moneyInternalTransferService.transfer(id, money, source, destination))
			.isEqualTo(transferOperation);
		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(id)
			.isEqualTo(transferOperation);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(2);

		movementFundsAssertions
			.extractingByKey(source)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(creditOperation);

		movementFundsAssertions
			.extractingByKey(destination)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(debitOperation);
	}

	@Test
	public void testTransferIdempotencyException() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "abc", new BigDecimal("12.21"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = topUpOperation.getId();
		val money = new Money(new BigDecimal("5.21"));
		val source = new Account("abc-test");
		val destination = new Account("cba-test");

		assertThatExceptionOfType(IdempotencyBadRequestException.class)
			.isThrownBy(() -> moneyInternalTransferService.transfer(id, money, source, destination))
			.withMessage("Details of current operation are differs with original one");

		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(id)
			.isEqualTo(topUpOperation);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(1);

		movementFundsAssertions
			.extractingByKey(source)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(topUpOperation.createDebitOperation());

		movementFundsAssertions
			.doesNotContainKey(destination);
	}

	@Test
	public void testTopUp() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "abc", new BigDecimal("99.99"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val expectedResult = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "qqq", new BigDecimal("20.01"));
		val debitOperation = expectedResult.createDebitOperation();
		debitOperation.setResult(OperationResult.COMPLETED);

		assertThat(moneyInternalTransferService.topUp(expectedResult.getId(), expectedResult.getAmount(), expectedResult.getDestination()))
			.isEqualTo(expectedResult);

		assertThat(uuidToOperationMap)
			.hasSize(2)
			.extractingByKey(expectedResult.getId())
			.isEqualTo(expectedResult);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(2);

		movementFundsAssertions
			.extractingByKey(expectedResult.getDestination())
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(debitOperation);
	}


	@Test
	public void testTopUpAlreadyCalculated() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "abc", new BigDecimal("99.99"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		assertThat(moneyInternalTransferService.topUp(topUpOperation.getId(), topUpOperation.getAmount(), topUpOperation.getDestination()))
			.isEqualTo(topUpOperation);

		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(topUpOperation.getId())
			.isEqualTo(topUpOperation);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(1);

		movementFundsAssertions
			.extractingByKey(topUpOperation.getDestination())
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(topUpOperation.createDebitOperation());
	}


	@Test
	public void testTopUpIdempotencyException() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val withdrawalOperation = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "abc", new BigDecimal("50.89"));

		uuidToOperationMap.put(withdrawalOperation.getId(), withdrawalOperation);
		accountToBalanceChangeMap.put(withdrawalOperation.getSource(), new CopyOnWriteArrayList<>(Collections.singletonList(withdrawalOperation.createCreditOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = withdrawalOperation.getId();
		val money = new Money(new BigDecimal("5.21"));
		val destination = new Account("cba-test");

		val topUpOperation = new TopUpOperation(id, money, destination);

		assertThatExceptionOfType(IdempotencyBadRequestException.class)
			.isThrownBy(() -> moneyInternalTransferService.topUp(topUpOperation.getId(), topUpOperation.getAmount(), topUpOperation.getDestination()))
			.withMessage("Details of current operation are differs with original one");

		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(topUpOperation.getId())
			.isEqualTo(withdrawalOperation);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(1);

		movementFundsAssertions
			.doesNotContainKey(topUpOperation.getDestination());
	}

	@Test
	public void testWithdraw() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "vvv", new BigDecimal("60.99"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = UUID.randomUUID();
		val money = new Money(new BigDecimal("15.99"));
		val source = new Account("vvv-test");
		val expectedResult = new WithdrawalOperation(id, money, source);
		val creditOperation = expectedResult.createCreditOperation();
		creditOperation.setResult(OperationResult.COMPLETED);

		assertThat(moneyInternalTransferService.withdraw(id, money, source))
			.isEqualTo(expectedResult);
		assertThat(uuidToOperationMap)
			.hasSize(2)
			.extractingByKey(id)
			.isEqualTo(expectedResult);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(1);

		movementFundsAssertions
			.extractingByKey(source)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(topUpOperation.createDebitOperation(), creditOperation);
	}


	@Test
	public void testWithdrawNotEnoughBalance() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "vvv", new BigDecimal("60.99"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = UUID.randomUUID();
		val money = new Money(new BigDecimal("15.99"));
		val source = new Account("qqq-test");
		val expectedResult = new WithdrawalOperation(id, money, source);
		val creditOperation = expectedResult.createCreditOperation();
		creditOperation.setResult(OperationResult.FAILED);

		assertThat(moneyInternalTransferService.withdraw(id, money, source))
			.isEqualTo(expectedResult);
		assertThat(uuidToOperationMap)
			.hasSize(2)
			.extractingByKey(id)
			.isEqualTo(expectedResult);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(2);

		movementFundsAssertions
			.extractingByKey(source)
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(creditOperation);
	}

	@Test
	public void testWithdrawAlreadyCalculated() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val withdrawalOperation = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "iii", new BigDecimal("29.99"));

		uuidToOperationMap.put(withdrawalOperation.getId(), withdrawalOperation);
		accountToBalanceChangeMap.put(withdrawalOperation.getSource(), new CopyOnWriteArrayList<>(Collections.singletonList(withdrawalOperation.createCreditOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		assertThat(moneyInternalTransferService.withdraw(withdrawalOperation.getId(), withdrawalOperation.getAmount(), withdrawalOperation.getSource()))
			.isEqualTo(withdrawalOperation);

		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(withdrawalOperation.getId())
			.isEqualTo(withdrawalOperation);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(1);

		movementFundsAssertions
			.extractingByKey(withdrawalOperation.getSource())
			.asList()
			.usingRecursiveFieldByFieldElementComparator()
			.containsExactly(withdrawalOperation.createCreditOperation());
	}

	@Test
	public void testWithdrawIdempotencyException() {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		val topUpOperation = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "vvv", new BigDecimal("60.99"));

		uuidToOperationMap.put(topUpOperation.getId(), topUpOperation);
		accountToBalanceChangeMap.put(topUpOperation.getDestination(), new CopyOnWriteArrayList<>(Collections.singletonList(topUpOperation.createDebitOperation())));

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val id = topUpOperation.getId();
		val money = new Money(new BigDecimal("15.99"));
		val source = new Account("qqq-test");

		assertThatExceptionOfType(IdempotencyBadRequestException.class)
			.isThrownBy(() -> moneyInternalTransferService.withdraw(id, money, source))
			.withMessage("Details of current operation are differs with original one");

		assertThat(uuidToOperationMap)
			.hasSize(1)
			.extractingByKey(id)
			.isEqualTo(topUpOperation);

		val movementFundsAssertions = assertThat(accountToBalanceChangeMap)
			.hasSize(1);

		movementFundsAssertions
			.doesNotContainKey(source);
	}

	@DataProvider
	public Object[][] findOperationDataProvider() {
		val topUpOperation = TestObjectsFactory.createOperation(OperationType.TOPUP, "qwe", new BigDecimal(100));
		val withdrawalOperation = TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "asd", new BigDecimal(20));
		val transfer = TestObjectsFactory.createOperation(OperationType.TRANSFER, "zxc", new BigDecimal(50));

		return new Object[][] {
			{Collections.emptyMap(), UUID.randomUUID(), null},
			{Map.of(topUpOperation.getId(), topUpOperation, withdrawalOperation.getId(), withdrawalOperation, transfer.getId(), transfer), UUID.randomUUID(), null},
			{Map.of(topUpOperation.getId(), topUpOperation, withdrawalOperation.getId(), withdrawalOperation, transfer.getId(), transfer), transfer.getId(), transfer},
			{Map.of(topUpOperation.getId(), topUpOperation), topUpOperation.getId(), topUpOperation},
			{Map.of(withdrawalOperation.getId(), withdrawalOperation), withdrawalOperation.getId(), withdrawalOperation},
		};
	}

	@Test(dataProvider = "findOperationDataProvider")
	public void testFindOperation(final Map<UUID, Operation> uuidToOperationMap, final UUID id, final Operation expectedOperation) {
		final Map<Account, List<BalanceChange>> accountToBalanceChangeMap = new HashMap<>();

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		val expectedResult = Optional.ofNullable(expectedOperation);
		assertThat(moneyInternalTransferService.findOperation(id))
			.isEqualTo(expectedResult);
	}

	@DataProvider
	public Object[][] getFundsMovementDataProvider() {
		val withdraw1 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "qwerty1", new BigDecimal("99.99"));
		val withdraw2 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "qwerty2", new BigDecimal("20.99"));
		val topUp1 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "qqq1", new BigDecimal("77.88"));
		val topUp2 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "qqq2", new BigDecimal("20.50"));

		val accountW1 =  withdraw1.getSource();
		val accountW2 =  withdraw2.getSource();
		val accountT1 =  topUp1.getDestination();
		val accountT2 =  topUp2.getDestination();
		val accountNotExist = new Account("null");

		return new Object[][] {
			{Collections.emptyMap(), accountNotExist, Collections.emptyList()},
			{Map.of(accountW1, Arrays.asList(withdraw1.createCreditOperation(), topUp1.createDebitOperation())), accountNotExist, Collections.emptyList()},
			{Map.of(accountW2, Arrays.asList(withdraw1.createCreditOperation(), topUp1.createDebitOperation()),
				accountT2, Arrays.asList(withdraw1.createCreditOperation(), topUp1.createDebitOperation(), withdraw2.createCreditOperation(), topUp2.createDebitOperation())),
				accountNotExist, Collections.emptyList()},
			{Map.of(accountT1, Arrays.asList(withdraw1.createCreditOperation(), topUp1.createDebitOperation()),
				accountT2, Arrays.asList(withdraw1.createCreditOperation(), topUp1.createDebitOperation(), withdraw2.createCreditOperation(), topUp2.createDebitOperation())),
				accountT1, Arrays.asList(withdraw1, topUp1)},
		};
	}

	@Test(dataProvider = "getFundsMovementDataProvider")
	public void testGetFundsMovement(final Map<Account, List<BalanceChange>> accountToBalanceChangeMap, final Account account,
		final List<Operation> expectedBalanceChanges) {
		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		assertThat(moneyInternalTransferService.getFundsMovement(account))
			.usingRecursiveFieldByFieldElementComparator()
			.containsAll(expectedBalanceChanges);
	}

	@DataProvider
	public Object[][] calculateBalanceDataProvider() {
		val withdraw1 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "qwerty1", new BigDecimal("99.99"));
		val withdraw2 = (WithdrawalOperation) TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "qwerty2", new BigDecimal("20.99"));
		val topUp1 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "qqq1", new BigDecimal("77.88"));
		val topUp2 = (TopUpOperation) TestObjectsFactory.createOperation(OperationType.TOPUP, "qqq2", new BigDecimal("20.50"));

		val accountW1 =  withdraw1.getSource();
		val accountW2 =  withdraw2.getSource();
		val accountT1 =  topUp1.getDestination();
		val accountNotExist = new Account("null");
		return new Object[][] {
			{Map.of(accountW1, Arrays.asList(withdraw1.createCreditOperation(), topUp2.createDebitOperation())), accountNotExist, BigDecimal.ZERO },
			{Collections.emptyMap(), accountNotExist, BigDecimal.ZERO },
			{Map.of(accountW2, Arrays.asList(withdraw2.createCreditOperation(), topUp1.createDebitOperation())), accountW2, new BigDecimal("56.89") },
			{Map.of(accountT1, Arrays.asList(withdraw1.createCreditOperation(), topUp1.createDebitOperation(), topUp2.createDebitOperation(), topUp2.createDebitOperation())),
				accountT1, new BigDecimal("18.89") },
		};
	}

	@Test(dataProvider = "calculateBalanceDataProvider")
	public void testCalculateBalance(final Map<Account, List<BalanceChange>> accountToBalanceChangeMap, final Account account,
		final BigDecimal expectedBalance) {

		final Map<UUID, Operation> uuidToOperationMap = new HashMap<>();

		moneyInternalTransferService = new MoneyInternalTransferServiceImpl(uuidToOperationMap, accountToBalanceChangeMap);

		assertThat(moneyInternalTransferService.calculateBalance(account))
			.isEqualTo(new Money(expectedBalance));
	}
}