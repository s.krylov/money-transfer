package com.home.challenge.money.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.home.challenge.money.TestObjectsFactory;
import com.home.challenge.money.pojo.Account;
import com.home.challenge.money.pojo.Money;
import com.home.challenge.money.pojo.Operation;
import com.home.challenge.money.pojo.OperationResult;
import com.home.challenge.money.pojo.OperationType;
import com.home.challenge.money.pojo.TopUpOperation;
import com.home.challenge.money.pojo.TransferOperation;
import com.home.challenge.money.pojo.WithdrawalOperation;
import com.home.challenge.money.rest.request.InternalFundsMovement;
import com.home.challenge.money.rest.request.OperationId;
import com.home.challenge.money.rest.request.TopUpBalance;
import com.home.challenge.money.rest.request.WithdrawalFunds;
import com.home.challenge.money.service.MoneyInternalTransferService;
import com.home.challenge.money.service.WithdrawalService;

import lombok.val;


public class MoneyTransferFacadeImplTest {
	private MoneyTransferFacadeImpl moneyTransferFacade;

	@DataProvider
	public Object[][] transferDataProvider() {
		return new Object[][] {
			{ new InternalFundsMovement(null, new Account("abc"), new Account("def"), new Money(new BigDecimal("12.21")))},
			{ new InternalFundsMovement(UUID.randomUUID(), new Account("qqq"), new Account("www"), new Money(new BigDecimal("20.00")))},
			{ new InternalFundsMovement(UUID.randomUUID(), new Account("test1"), new Account("test2"), new Money(new BigDecimal("00.01")))},
		};
	}

	@Test(dataProvider = "transferDataProvider")
	public void testTransfer(final InternalFundsMovement internalFundsMovement) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		val id = internalFundsMovement.getId() == null ? UUID.randomUUID() : internalFundsMovement.getId();
		val result = new TransferOperation(id, internalFundsMovement.getMoney(), internalFundsMovement.getSource(),
			internalFundsMovement.getDestination());
		doReturn(result).when(moneyInternalTransferService).transfer(any(), eq(internalFundsMovement.getMoney()),
			eq(internalFundsMovement.getSource()), eq(internalFundsMovement.getDestination()));
		assertThat(moneyTransferFacade.transfer(internalFundsMovement))
			.isEqualTo(result);
		verify(moneyInternalTransferService, times(1)).transfer(any(), eq(internalFundsMovement.getMoney()),
			eq(internalFundsMovement.getSource()), eq(internalFundsMovement.getDestination()));
	}

	@DataProvider
	public Object[][] transferValidationFailedDataProvider() {
		return new Object[][] {
			{ new InternalFundsMovement(null, null, null, null), "Source account is required"},
			{ new InternalFundsMovement(UUID.randomUUID(), null, new Account("def"), new Money(new BigDecimal("12.21"))), "Source account is required"},
			{ new InternalFundsMovement(UUID.randomUUID(), null, null, null), "Source account is required"},
			{ new InternalFundsMovement(null, new Account("def"), null, new Money(new BigDecimal("12.21"))), "Destination account is required"},
			{ new InternalFundsMovement(UUID.randomUUID(), new Account("def"), new Account("def "), new Money(BigDecimal.ONE)), "Source and destination are the same"},
			{ new InternalFundsMovement(null, new Account("      def"), new Account(" def "), new Money(BigDecimal.ONE)), "Source and destination are the same"},
			{ new InternalFundsMovement(UUID.randomUUID(), new Account("def"), new Account("abc"), new Money(BigDecimal.ZERO)), "Money amount should be positive"},
			{ new InternalFundsMovement(null, new Account("def"), new Account("abc"), new Money(new BigDecimal(-10))), "Money amount should be positive"},
			{ new InternalFundsMovement(UUID.randomUUID(), new Account("def"), new Account("abc"), null), "Money amount should be positive"},
			{ new InternalFundsMovement(null, new Account("def"), new Account("abc"), null), "Money amount should be positive"},
		};
	}

	@Test(dataProvider = "transferValidationFailedDataProvider")
	public void testTransferValidationFailed(final InternalFundsMovement internalFundsMovement, final String expectedErrorMessage) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> moneyTransferFacade.transfer(internalFundsMovement))
			.withMessage(expectedErrorMessage);
		verify(moneyInternalTransferService, never()).transfer(any(), any(), any(), any());
	}

	@DataProvider
	public Object[][] topUpDataProvider() {
		return new Object[][] {
			{ new TopUpBalance(null, new Account("abc"), new Money(new BigDecimal("25.00")))},
			{ new TopUpBalance(null, new Account("  def "), new Money(new BigDecimal("3.99")))},
			{ new TopUpBalance(UUID.randomUUID(), new Account("qqq"), new Money(new BigDecimal("70.00")))},
			{ new TopUpBalance(UUID.randomUUID(), new Account("  www "), new Money(new BigDecimal("3.14")))},
		};
	}

	@Test(dataProvider = "topUpDataProvider")
	public void testTopUp(final TopUpBalance topUpBalance) {

		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		val id = topUpBalance.getId() == null ? UUID.randomUUID() : topUpBalance.getId();
		val result = new TopUpOperation(id, topUpBalance.getMoney(), topUpBalance.getDestination());
		doReturn(result).when(moneyInternalTransferService).topUp(any(), eq(topUpBalance.getMoney()), eq(topUpBalance.getDestination()));
		assertThat(moneyTransferFacade.topUp(topUpBalance))
			.isEqualTo(result);
		verify(moneyInternalTransferService, times(1)).topUp(any(), eq(topUpBalance.getMoney()),
			eq(topUpBalance.getDestination()));
	}

	@DataProvider
	public Object[][] topUpValidationFailedDataProvider() {
		return new Object[][] {
			{ new TopUpBalance(null, new Account(null), new Money(new BigDecimal("25.00"))), "Destination account is required"},
			{ new TopUpBalance(UUID.randomUUID(), null, new Money(new BigDecimal("10.50"))), "Destination account is required"},
			{ new TopUpBalance(null, null, new Money(null)), "Destination account is required"},
			{ new TopUpBalance(null, new Account("  def "), new Money(null)), "Money amount should be positive"},
			{ new TopUpBalance(UUID.randomUUID(), new Account(" abc "), null), "Money amount should be positive"},
			{ new TopUpBalance(null, new Account("qqq"), new Money(BigDecimal.ZERO)), "Money amount should be positive"},
			{ new TopUpBalance(UUID.randomUUID(), new Account("www"), new Money(new BigDecimal("-15.00"))), "Money amount should be positive"},
		};
	}

	@Test(dataProvider = "topUpValidationFailedDataProvider")
	public void testTopUpValidationFailed(final TopUpBalance topUpBalance, final String expectedErrorMessage) {

		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> moneyTransferFacade.topUp(topUpBalance))
			.withMessage(expectedErrorMessage);
		verify(moneyInternalTransferService, never()).topUp(any(), any(), any());
	}

	@DataProvider
	public Object[][] withdrawDataProvider() {
		return new Object[][] {
			{ new WithdrawalFunds(null, new Account("abc"), "address123", new Money(new BigDecimal("25.00"))),
				OperationResult.COMPLETED, 1},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account("abc1"), "address234", new Money(new BigDecimal("25.01"))),
				OperationResult.COMPLETED, 1},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account("abc2"), "address345", new Money(new BigDecimal("25.20"))),
				OperationResult.PROCESSING, 1}, // not possible
			{ new WithdrawalFunds(null, new Account("abc3"), "address456", new Money(new BigDecimal("26.00"))),
				OperationResult.FAILED, 0},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account("abc4"), "address567", new Money(new BigDecimal("26.55"))),
				OperationResult.FAILED, 0},
		};
	}

	@Test(dataProvider = "withdrawDataProvider")
	public void testWithdraw(final WithdrawalFunds withdrawalFunds, final OperationResult internalServiceMockedResult,
		final int expectedWithdrawalServiceCall) {

		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		val id = withdrawalFunds.getId() == null ? UUID.randomUUID() : withdrawalFunds.getId();
		val result = new WithdrawalOperation(id, withdrawalFunds.getMoney(), withdrawalFunds.getSource());
		result.addResult(internalServiceMockedResult);

		doNothing().when(withdrawalService).requestWithdrawal(any(), eq(new WithdrawalService.Address(withdrawalFunds.getAddress())),
			eq(withdrawalFunds.getMoney()));
		doReturn(result).when(moneyInternalTransferService).withdraw(any(), eq(withdrawalFunds.getMoney()), eq(withdrawalFunds.getSource()));

		assertThat(moneyTransferFacade.withdraw(withdrawalFunds))
			.isEqualTo(result);
		verify(moneyInternalTransferService, times(1)).withdraw(any(), eq(withdrawalFunds.getMoney()),
			eq(withdrawalFunds.getSource()));

		verify(withdrawalService, times(expectedWithdrawalServiceCall)).requestWithdrawal(any(), eq(new WithdrawalService.Address(withdrawalFunds.getAddress())),
			eq(withdrawalFunds.getMoney()));
		verify(withdrawalService, never()).getRequestState(any());
	}

	@DataProvider
	public Object[][] withdrawValidationFailedDataProvider() {
		return new Object[][] {
			{ new WithdrawalFunds(null, new Account(null), "address1", new Money(new BigDecimal("25.00"))), "Source account is required"},
			{ new WithdrawalFunds(UUID.randomUUID(), null,"address2", new Money(new BigDecimal("10.50"))), "Source account is required"},
			{ new WithdrawalFunds(UUID.randomUUID(), null,"", new Money(null)), "Source account is required"},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account("qwerty")," ", new Money(new BigDecimal("10.50"))), "Withdrawal address is required"},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account("asdfgh"),null, new Money(new BigDecimal("10.50"))), "Withdrawal address is required"},
			{ new WithdrawalFunds(null, new Account("  def "), "address3", new Money(null)), "Money amount should be positive"},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account(" abc "),  "address4",null), "Money amount should be positive"},
			{ new WithdrawalFunds(null, new Account("qqq"), "address5", new Money(BigDecimal.ZERO)), "Money amount should be positive"},
			{ new WithdrawalFunds(UUID.randomUUID(), new Account("www"), "address6", new Money(new BigDecimal("-15.00"))), "Money amount should be positive"},
		};
	}

	@Test(dataProvider = "withdrawValidationFailedDataProvider")
	public void testWithdrawValidationFailed(final WithdrawalFunds withdrawalFunds, final String expectedErrorMessage) {

		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> moneyTransferFacade.withdraw(withdrawalFunds))
			.withMessage(expectedErrorMessage);
		verify(moneyInternalTransferService, never()).withdraw(any(), any(), any());
		verify(withdrawalService, never()).requestWithdrawal(any(), any(), any());

		verify(withdrawalService, never()).getRequestState(any());
	}

	@DataProvider
	public Object[][] findOperationDataProvider() {
		return new Object[][] {
			{ new OperationId(UUID.randomUUID()), TestObjectsFactory.createOperation(OperationType.TOPUP, "123", new BigDecimal("2.13")) },
			{ new OperationId(UUID.randomUUID()), null },
		};
	}

	@Test(dataProvider = "findOperationDataProvider")
	public void testFindOperation(final OperationId id, final Operation mockedOperation) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		val mockedFindResult = Optional.ofNullable(mockedOperation);
		doReturn(mockedFindResult).when(moneyInternalTransferService).findOperation(id.getId());

		assertThat(moneyTransferFacade.findOperation(id))
			.isEqualTo(mockedFindResult);

		verify(moneyInternalTransferService, times(1)).findOperation(id.getId());
	}

	@Test
	public void testFindOperationValidationFailed() {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		assertThatExceptionOfType(NullPointerException.class)
			.isThrownBy(() -> moneyTransferFacade.findOperation(new OperationId(null)))
			.withMessage("id is required");
		verify(moneyInternalTransferService, never()).findOperation(any());
	}

	@DataProvider
	public Object[][] getFundsMovementDataProvider() {
		return new Object[][] {
			{ new Account(" 234 "), Arrays.asList(
				TestObjectsFactory.createOperation(OperationType.TOPUP, "234", new BigDecimal("10.00")),
				TestObjectsFactory.createOperation(OperationType.WITHDRAWAL, "234", new BigDecimal("5.00"))
				)},
			{ new Account("abc"), Collections.emptyList() },
		};
	}

	@Test(dataProvider = "getFundsMovementDataProvider")
	public void testGetFundsMovement(final Account account, final List<Operation> mockedOperations) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		doReturn(mockedOperations).when(moneyInternalTransferService).getFundsMovement(account);

		assertThat(moneyTransferFacade.getFundsMovement(account))
			.isEqualTo(mockedOperations);
		verify(moneyInternalTransferService, times(1)).getFundsMovement(account);
	}

	@DataProvider
	public Object[][] getFundsMovementValidationFailedDataProvider() {
		return new Object[][] {
			{ new Account(null), "Account is required"},
			{ new Account(" "), "Account is required"},
			{ new Account(""), "Account is required"},
			{ null, "Account is required"},
		};
	}

	@Test(dataProvider = "getFundsMovementValidationFailedDataProvider")
	public void testGetFundsMovementValidationFailed(final Account account, final String expectedErrorMessage) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> moneyTransferFacade.getFundsMovement(account))
			.withMessage(expectedErrorMessage);
		verify(moneyInternalTransferService, never()).getFundsMovement(any());
	}

	@DataProvider
	public Object[][] calculateBalanceDataProvider() {
		return new Object[][] {
			{ new Account("123"), new Money(new BigDecimal("11.22"))},
			{ new Account("asd"), new Money(BigDecimal.ZERO)},
		};
	}

	@Test(dataProvider = "calculateBalanceDataProvider")
	public void testCalculateBalance(final Account account, final Money mockedResult) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		doReturn(mockedResult).when(moneyInternalTransferService).calculateBalance(account);

		assertThat(moneyTransferFacade.calculateBalance(account))
			.isEqualTo(mockedResult);
		verify(moneyInternalTransferService, times(1)).calculateBalance(account);
	}

	@DataProvider
	public Object[][] calculateBalanceValidationFailedDataProvider() {
		return new Object[][] {
			{ new Account(null), "Account is required"},
			{ new Account(" "), "Account is required"},
			{ new Account(""), "Account is required"},
			{ null, "Account is required"},
		};
	}

	@Test(dataProvider = "calculateBalanceValidationFailedDataProvider")
	public void testCalculateBalanceValidationFailed(final Account account, final String expectedErrorMessage) {
		val moneyInternalTransferService = mock(MoneyInternalTransferService.class);
		val withdrawalService = mock(WithdrawalService.class);
		moneyTransferFacade = new MoneyTransferFacadeImpl(moneyInternalTransferService, withdrawalService);

		assertThatExceptionOfType(IllegalArgumentException.class)
			.isThrownBy(() -> moneyTransferFacade.calculateBalance(account))
			.withMessage(expectedErrorMessage);
		verify(moneyInternalTransferService, never()).calculateBalance(any());
	}
}